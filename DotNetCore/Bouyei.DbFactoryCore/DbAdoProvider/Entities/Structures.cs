﻿/*-------------------------------------------------------------
 *   auth: bouyei
 *   date: 2017/7/12 22:55:33
 *contact: 453840293@qq.com
 *profile: www.openthinking.cn
 *   guid: 6bceaafe-f915-41b8-87c5-f439de40ab16
---------------------------------------------------------------*/
using System;
using System.Data;
using System.Data.Common;

namespace Bouyei.DbFactoryCore.DbAdoProvider
{
    internal class AssemblyFactoryInfo
    {
        public string FactoryName { get; set; }

        public string InvariantName { get; set; }

        public string Culture { get; set; }

        public string Version { get; set; }

        public string PublicKeyToken { get; set; }

        public string AssemblyName { get; private set; }

        public AssemblyFactoryInfo()
        { }

        public AssemblyFactoryInfo(string AssemblyFullName)
        {
            this.AssemblyName = AssemblyFullName;
            string[] infos = AssemblyFullName.Split(',');
            this.InvariantName = infos[0];
            this.Version = infos[1].Split('=')[1];
            this.Culture = infos[2].Split('=')[1];
            this.PublicKeyToken = infos[3].Split('=')[1];
        }

        public override string ToString()
        {
            return string.Format("{0},{1},Version={2},Culture={3},PublicKeyToken={4}",
                FactoryName,
                InvariantName,
                Version,
                Culture,
                PublicKeyToken);
        }
    }
}
